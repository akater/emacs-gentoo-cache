;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'gentoo-cache
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("gentoo-cache-core"
                       "gentoo-cache-sets"
                       "gentoo-cache")
  site-lisp-config-prefix "50"
  license "GPL-3")
